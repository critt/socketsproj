#include "defns.h"

#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

#define ECHOMAX 255     /* Longest string to echo */

void DieWithError(const char *errorMessage) /* External error handling function */
{
    perror(errorMessage);
    exit(1);
}

int main(int argc, char *argv[])
{
    int sock;                        /* Socket descriptor */
    struct sockaddr_in echoServAddr; /* Echo server address */
    struct sockaddr_in fromAddr;     /* Source address of echo */
    unsigned short echoServPort;     /* Echo server port */
    unsigned int fromSize;           /* In-out of address size for recvfrom() */
    char *servIP;                    /* IP address of server */
    int respStringLen;               /* Length of received response */


    //initialize request
    struct request req;
    req.requestCode = REGISTER;
    strncpy(req.contactListName, " ", sizeof req.contactListName);
    strncpy(req.contactName, " ", sizeof req.contactName);
    strncpy(req.ipAddress, " ", sizeof req.ipAddress);
    strncpy(req.port, " ", sizeof req.port);
    memset(req.fileName, 0, sizeof req.fileName);

    struct response* resp;
    resp = malloc(sizeof *resp);

    //parse input and craft appropriate request
    if(argc == 4){
        printf("4\n");
        if(strcmp(argv[3], "query-lists") == 0){
            req.requestCode = QUERY;
            strncpy(req.contactListName, " ", sizeof req.contactListName);
            strncpy(req.contactName, " ", sizeof req.contactName);
            strncpy(req.ipAddress, " ", sizeof req.ipAddress);
            strncpy(req.port, " ", sizeof req.port);
            memset(req.fileName, 0, sizeof req.fileName);
        }
        else {
            fprintf(stderr,"Usage: %s <Server IP> [<Server Port>] <Params>\n", argv[0]);
            exit(1);
        }
    }
    else if(argc == 5){
        printf("5\n");
        if(strcmp(argv[3], "register") == 0){
            req.requestCode = REGISTER;
            strncpy(req.contactListName, argv[4], sizeof req.contactListName);
            strncpy(req.contactName, " ", sizeof req.contactName);
            strncpy(req.ipAddress, " ", sizeof req.ipAddress);
            strncpy(req.port, " ", sizeof req.port);
            memset(req.fileName, 0, sizeof req.fileName);
        }
        else if(strcmp(argv[3], "save") == 0){
            req.requestCode = SAVE_FILE;
            strncpy(req.contactListName, " ", sizeof req.contactListName);
            strncpy(req.contactName, " ", sizeof req.contactName);
            strncpy(req.ipAddress, " ", sizeof req.ipAddress);
            strncpy(req.port, " ", sizeof req.port);
            strncpy(req.fileName, argv[4], sizeof req.fileName);
        }
        else {
            fprintf(stderr,"Usage: %s <Server IP> [<Server Port>] <Params>\n", argv[0]);
            exit(1);
        }
    }
    else if(argc == 6){
        printf("6\n");
        if(strcmp(argv[3], "leave") == 0){
            req.requestCode = LEAVE;
            strncpy(req.contactListName, argv[4], sizeof req.contactListName);
            strncpy(req.contactName, argv[5], sizeof req.contactName);
            strncpy(req.ipAddress, " ", sizeof req.ipAddress);
            strncpy(req.port, " ", sizeof req.port);
            memset(req.fileName, 0, sizeof req.fileName);
        }
        else {
            fprintf(stderr,"Usage: %s <Server IP> [<Server Port>] <Params>\n", argv[0]);
            exit(1);
        }
    }
    else if(argc == 8){
        if(strcmp(argv[3], "join") == 0){
            req.requestCode = JOIN;
            strncpy(req.contactListName, argv[4], sizeof req.contactListName);
            strncpy(req.contactName, argv[5], sizeof req.contactName);
            strncpy(req.ipAddress, argv[6], sizeof req.ipAddress);
            strncpy(req.port, argv[7], sizeof req.port);
            memset(req.fileName, 0, sizeof req.fileName);
        }
        else {
            fprintf(stderr,"Usage: %s <Server IP> [<Server Port>] <Params>\n", argv[0]);
            exit(1);
        }
    }
    else{
        fprintf(stderr,"Usage: %s <Server IP> [<Server Port>] <Params>\n", argv[0]);
        exit(1);
    }


    servIP = argv[1];           /* First arg: server IP address (dotted quad) */
    echoServPort = atoi(argv[2]);       /* Second arg: string to echo */

    /* Create a datagram/UDP socket */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        DieWithError("socket() failed");

    /* Construct the server address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));    /* Zero out structure */
    echoServAddr.sin_family = AF_INET;                 /* Internet addr family */
    echoServAddr.sin_addr.s_addr = inet_addr(servIP);  /* Server IP address */
    echoServAddr.sin_port   = htons(echoServPort);     /* Server port */


	for( int i = 0; i < 1; i++ )
	{

        // Send the request to the server
        if (sendto(sock, &req, sizeof(struct request), 0, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) != sizeof(struct request))
            DieWithError("sendto() sent a different number of bytes than expected");
  
        printf(">>Server receives request: requestCode=%s, contactListName=%s, contactName=%s, ipAddress=%s, port=%s, fileName=%s\n", codeParse(req.requestCode), req.contactListName, req.contactName, req.ipAddress, req.port, req.fileName );
  

        // Recv a response from the server and output appropriate information about the success/failure of the request
        fromSize = sizeof(fromAddr);
        if ((respStringLen = recvfrom(sock, resp, sizeof(*resp), 0, (struct sockaddr *) &fromAddr, &fromSize)) != sizeof(*resp))
            DieWithError("recvfrom() failed");

        if (echoServAddr.sin_addr.s_addr != fromAddr.sin_addr.s_addr)
        {
            fprintf(stderr,"Error: received a packet from unknown source.\n");
            exit(1);
        }
        if(req.requestCode == REGISTER || req.requestCode ==  JOIN || req.requestCode ==  LEAVE || req.requestCode == SAVE_FILE)
            printf(">>Client received response from server: requestCode=%s, responseCode=%s\n", codeParse(resp->requestCode), codeParse(resp->responseCode) );    // Print the echoed arg
        else if(req.requestCode == QUERY){
            printf(">>Client received response from server: requestCode=%s, responseCode=%d\n", codeParse(resp->requestCode), resp->responseCode );
            if(resp->responseCode > 0){
                printf("Contact list names: \n");
                for(int i = 0; i < resp->responseCode; i++){
                    printf("%s\n", resp->contactListNames[i]);
                }
            }
        }
	}
    
    close(sock);
    exit(0);
}
