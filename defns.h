//lengths
#define MAX_CONTACT_LISTS 25
#define MAX_NAME_LEN 25
#define MAX_CONTACTS 25
#define PORT_LEN 6

//response codes
#define FAILURE 1000
#define SUCCESS 1001

//request codes
#define QUERY 2000
#define REGISTER 2001
#define JOIN 2002
#define LEAVE 2003
#define IM 2004
#define SAVE_FILE 2005

struct request
{
	int requestCode;
	char contactListName[MAX_NAME_LEN];
	char contactName[MAX_NAME_LEN];
	char ipAddress[MAX_NAME_LEN];
	char port[PORT_LEN];
	char fileName[MAX_NAME_LEN];
};

struct response
{
	int requestCode;
	int responseCode;
	char contactListNames[MAX_CONTACT_LISTS][MAX_NAME_LEN];
	//list of contacts
};

struct client
{
	char name[MAX_NAME_LEN];
	char ip[MAX_NAME_LEN];
	char port[PORT_LEN];
};

struct contactList
{
	char contactListName[MAX_NAME_LEN];
	struct client clients[MAX_CONTACTS];
	int count;
};

struct ledger
{
	struct contactList allLists[MAX_CONTACT_LISTS];

};

char* codeParse(int code){
	char* out;
	switch (code){
		case QUERY:
        out = "QUERY";
        break;
    case REGISTER:
        out = "REGISTER";
        break;
    case JOIN:
        out = "JOIN";
        break;
    case LEAVE:
        out = "LEAVE";
        break;
    case IM:
        out = "IM";
        break;
    case SAVE_FILE:
        out = "SAVE_FILE";
        break;
    case SUCCESS:
    	out = "SUCCESS";
    	break;
    case FAILURE:
    	out = "FAILURE";
    	break;
	}
	return out;
}