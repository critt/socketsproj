#include "defns.h"

#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket() and bind() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

#define ECHOMAX 255     /* Longest string to echo */

void DieWithError(const char *errorMessage) /* External error handling function */
{
    perror(errorMessage);
    exit(1);
}

int main(int argc, char *argv[])
{
    int sock;                        /* Socket */
    struct sockaddr_in echoServAddr; /* Local address */
    struct sockaddr_in echoClntAddr; /* Client address */
    unsigned int cliAddrLen;         /* Length of incoming message */
    //char echoBuffer[ECHOMAX];        /* Buffer for echo string */
    unsigned short echoServPort;     /* Server port */
    int recvMsgSize;                 /* Size of received message */
    struct response resp;
    struct ledger ledg;
    struct request* req;
    int contactListCount = 0;
    int wat = 0;

    req = malloc(sizeof *req);

    if (argc != 2)         /* Test for correct number of parameters */
    {
        fprintf(stderr,"Usage:  %s <UDP SERVER PORT>\n", argv[0]);
        exit(1);
    }

    echoServPort = atoi(argv[1]);  /* First arg:  local port */

    /* Create socket for sending/receiving datagrams */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        DieWithError("socket() failed");

    /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
    echoServAddr.sin_family = AF_INET;                /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    echoServAddr.sin_port = htons(echoServPort);      /* Local port */

    /* Bind to the local address */
    if (bind(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
        DieWithError("bind() failed");


    //initialize contact list from file
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    int initNumContactLists = 0;

    fp = fopen("init.txt", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    getline(&line, &len, fp);
    initNumContactLists = atoi(line);
    for(int i = 0; i < initNumContactLists; i++){
        getline(&line, &len, fp);
        char * pch;
        //printf ("Splitting string %s into tokens:\n", line);
        pch = strtok (line," ");

        struct contactList cl;
        strncpy(cl.contactListName, pch, sizeof cl.contactListName);
        printf("Creating contact list: %s\n", cl.contactListName);
        cl.count = atoi( strtok (NULL, " "));
        //printf("Contacts to add: %d\n", cl.count);
        for(int j = 0; j < cl.count; j++){
            struct client c;
            getline(&line, &len, fp);
            pch = strtok (line," ");
            strncpy(c.name, pch, sizeof c.name);
            pch = strtok (NULL," ");
            strncpy(c.ip, pch, sizeof c.ip);
            pch = strtok (NULL," ");
            strncpy(c.port, pch, strlen(pch) - 1);
            cl.clients[j] = c;
            printf("added client %s %s %s to contact list %s\n", c.name, c.ip, c.port, cl.contactListName);
        }
        ledg.allLists[contactListCount] = cl;
        printf("added contact list %s to ledger\n", ledg.allLists[contactListCount].contactListName);
        contactListCount++;
    }

    fclose(fp);
    if (line)
        free(line);


    for (;;) // Run forever
    {
        // Set the size of the in-out parameter
        cliAddrLen = sizeof(echoClntAddr);

        // Block until receive message from a client
        if ((recvMsgSize = recvfrom(sock, req, sizeof(*req), 0, (struct sockaddr *) &echoClntAddr, &cliAddrLen)) < 0)
            DieWithError("recvfrom() failed");

        printf(">>Server receives request: requestCode=%s, contactListName=%s, contactName=%s, ipAddress=%s, port=%s, fileName=%s\n", codeParse(req->requestCode), req->contactListName, req->contactName, req->ipAddress, req->port, req->fileName );
        resp.requestCode = req->requestCode;
        switch (resp.requestCode){

            case QUERY:
                resp.responseCode = contactListCount;
                for(int i = 0; i < contactListCount; i++){
                    strcpy(resp.contactListNames[i], ledg.allLists[i].contactListName);
                }
                break;

            case REGISTER:
                resp.responseCode = SUCCESS;
                //check if the contact list container is full, or if the contact list name already exists
                for(int i = 0; i < contactListCount; i++){
                    if((strcmp(ledg.allLists[i].contactListName, req->contactListName) == 0) || contactListCount >= MAX_CONTACT_LISTS){
                        resp.responseCode = FAILURE;
                        i = contactListCount;
                        printf("Unable to create contact list: %s\n", req->contactListName);
                    }
                }
                //if able to add the contact list, do it
                if(resp.responseCode == SUCCESS){
                    struct contactList cl;
                    strncpy(cl.contactListName, req->contactListName, sizeof req->contactListName);
                    cl.count = 0;
                    ledg.allLists[contactListCount] = cl;
                    contactListCount++;
                    printf("Created contact list: %s\n", req->contactListName);
                }
                break;

            case JOIN:
                resp.responseCode = SUCCESS;
                int foundContactList = 0;
                //find correct contact list
                for(int i = 0; i < contactListCount; i++){
                    //when contact list found
                    if(strcmp(ledg.allLists[i].contactListName, req->contactListName) == 0){
                        foundContactList = 1;
                        //check if the contact name allready exists in that contact list or if the contact list is full
                        for(int j = 0; j < ledg.allLists[i].count; j++){
                            //when duplicate contact found or contact list full
                            if((strcmp(ledg.allLists[i].clients[j].name, req->contactName) == 0) || ledg.allLists[i].count >= MAX_CONTACTS){
                                //reaching here means that either the contact list is full, or the contact already exists in the contact list 
                                resp.responseCode = FAILURE;
                                j = ledg.allLists[i].count;
                                i = contactListCount;
                                 printf("Unable to add contact %s to contact list %s\n", req->contactName, req->contactListName);
                            }
                        }
                        //if we found the contact list and we are able to add the contact to it, do it
                        if(resp.responseCode == SUCCESS){
                            struct client c;
                            strncpy(c.name, req->contactName, sizeof req->contactName);
                            strncpy(c.ip, req->ipAddress, sizeof req->ipAddress);
                            strncpy(c.port, req->port, sizeof req->port);
                            ledg.allLists[i].clients[ledg.allLists[i].count] = c;
                            ledg.allLists[i].count++;
                            printf("Added %s to contact list %s\n", req->contactName, req->contactListName);
                             printf("Contents of contact list %s:\n", req->contactListName);
                                for(int h = 0; h < ledg.allLists[i].count; h++){
                                    printf("%s\n", ledg.allLists[i].clients[h].name);
                                }
                        }
                    }
                }
                //if contact list not found
                if(foundContactList == 0){
                    printf("Contact list %s not found\n", req->contactListName);
                    resp.responseCode = FAILURE;
                }
                break;

            case LEAVE:
                resp.responseCode = FAILURE;
                foundContactList = 0;
                int foundContact = 0;
                //find correct contact list
                for(int i = 0; i < contactListCount; i++){
                    //when contact list found
                    if(strcmp(ledg.allLists[i].contactListName, req->contactListName) == 0){
                        foundContactList = 1;
                        //check if the contact name exists in that contact list
                        for(int j = 0; j < ledg.allLists[i].count; j++){
                            //when contact found
                            if(strcmp(ledg.allLists[i].clients[j].name, req->contactName) == 0){
                                foundContact = 1;
                                //reaching here means that the contact exists in the contact list 
                                //remove it from the contact list here
                                
                                for(int k = j; k < MAX_CONTACTS; k++){
                                    ledg.allLists[i].clients[k] = ledg.allLists[i].clients[k + 1];

                                }

                                resp.responseCode = SUCCESS;
                                ledg.allLists[i].count--;
                                printf("Removed contact %s from contact list %s\n", req->contactName, req->contactListName);
                                printf("Contents of contact list %s:\n", req->contactListName);
                                for(int h = 0; h < ledg.allLists[i].count; h++){
                                    printf("%s\n", ledg.allLists[i].clients[h].name);
                                }

                                //get out
                                j = ledg.allLists[i].count;
                                i = contactListCount;
                            }
                        }
                    }
                }
                //if contact list not found
                if(foundContactList == 0){
                    printf("Contact list %s not found\n", req->contactListName);
                    resp.responseCode = FAILURE;
                }
                //if conteact not found
                if(foundContact == 0){
                     printf("Contact %s not found\n", req->contactName);
                    resp.responseCode = FAILURE;
                }
                break;
                
            case IM:
                printf("im\n");
                break;

            case SAVE_FILE:

                resp.responseCode = SUCCESS;
                wat = 1;
                FILE * f;
                f = fopen(req->fileName, "a");
                if (f == NULL)
                {
                    resp.responseCode = FAILURE;
                    printf("Error opening file!\n");
                }


                if(resp.responseCode == SUCCESS){
                    fprintf(f, "%d\n", contactListCount);
                    for(int i = 0; i < contactListCount; i++){
                        fprintf(f, "%s %d\n", ledg.allLists[i].contactListName, ledg.allLists[i].count);
                        for(int j = 0; j < ledg.allLists[i].count; j++){
                            fprintf(f, "%s %s %s\n", ledg.allLists[i].clients[j].name, ledg.allLists[i].clients[j].ip, ledg.allLists[i].clients[j].port);
                        }
                    }
                    printf("Ledger saved\n");
                }
                
                fclose(f);
                break;
        }
        

        // Send response datagram back to the client
        if (sendto(sock, &resp, sizeof(struct response), 0, (struct sockaddr *) &echoClntAddr, sizeof(echoClntAddr)) != sizeof(struct response))
            DieWithError("sendto() sent a different number of bytes than expected");
    }
    // NOT REACHED
}
